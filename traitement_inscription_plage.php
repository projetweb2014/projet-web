<?php session_start()?>
<!DOCTYPE php>
<html>
    <head>
        <title>Traitement</title>
        <meta charset="utf-8" />
		<link rel="stylesheet" href="css/reset.css">
		<link rel="stylesheet" href="css/style.css">
    </head>
    <body>
		<?php include('includes/header.inc.php'); ?>
		<?php include('includes/bdd_inscription_plage.inc.php'); ?>
        <h2>Inscription en cours</h2>
        <p>
            Votre inscription à la plage <?php echo htmlentities($_POST['nom_plage']); ?> est en cours. <br /> Merci de patientez. 
			<?php 
				if(isset($_SESSION['droit']) && $_SESSION['droit'] == '0'){
					echo "'<script>setTimeout('top.location = \'page_admin.php\'', 1000);</script>'";
				}
				else{
					echo "'<script>setTimeout('top.location = \'page_users.php\'', 1000);</script>'";
				}
			?>
	<?php 
		include('includes/footer.inc.php');
	?>
        </p>
    </body>
</html>