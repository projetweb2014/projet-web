<!DOCTYPE php>
<html>
    <head>
        <title>Traitement</title>
        <meta charset="utf-8" />
		<meta http-equiv="refresh" content="1;URL='./page_admin.php'" />
		<link rel="stylesheet" href="css/reset.css">
		<link rel="stylesheet" href="css/style.css">
    </head>
    <body>
		<?php include('includes/header.inc.php'); ?>
		<?php include('includes/bdd_creation_plage.inc.php'); ?>
        <h2>Création en cours </h2>
        <p>
            La création de votre plage est en cours <?php echo htmlentities($_POST['nom_plage']);?> . <br /> Merci de patientez. 
        </p>
    <?php 
		include('includes/footer.inc.php');
    ?>
    </body>
</html>