CREATE TABLE `plage` (
   `id` int(10) unsigned not null auto_increment,
   `nom_plage` varchar(50),
   `Descriptif` varchar(7000),
   PRIMARY KEY (`id`),
   UNIQUE KEY (`nom_plage`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

INSERT INTO `plage` (`id`, `nom_plage`, `Descriptif`) VALUES 
('1', 'Le Havre', 'Constituée de sable et de galets, la plage du Havre recèle une vie intense. On y rencontre le plus souvent des espèces fouisseuses (couteaux, palourdes...) ou tubicoles (qui construisent un tube dans lequel ils vivent, comme les vers arénicoles). Lorsque la marée descend, le rivage dévoile ces nombreux trésors : moules, bigorneaux, crabes verts, étrilles et crevettes très convoités par les pêcheurs à pied.');

CREATE TABLE `users` (
   `id` int(3) unsigned not null auto_increment,
   `pseudo` varchar(30),
   `motdepasse` varchar(35),
   `plage_id` int(3) unsigned,
   `droit` int(11) not null,
   PRIMARY KEY (`id`),
   UNIQUE KEY (`pseudo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;