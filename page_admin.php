<?php session_start()?>
<!Doctype html>
<html>
  <head>
    <title>Nettoyage de plage : ADMIN PAGE</title>
    <meta charset ="utf-8"/>
	<link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/style.css">
	<script type="text/javascript" src ="js/jquery.js"></script>
	<script type="text/javascript" src ="js/javascript.js"></script>
  </head>
  <body>
   <?php 
		if(!(isset($_SESSION['id'])) && !(isset($_SESSION['droit'])) && ($_SESSION['droit'] !='0')) {
			echo '<script> redirect_index_direct()</script>';
		}
   ?>
  <?php include('includes/header.inc.php'); ?>
    <div class="contenu">
		  <nav>
			  <ul id="Menu">
				<li><div class="fake_button" onclick="login_form()"><?php echo "Profil ".$_SESSION['id']?> </div></li>
				<li><div class="fake_button" onclick="creation_plage_form()">Création Plage</div></li>
				<li><div class="fake_button" onclick="inscription_plage_form()">Inscription Plage</div></li>
				<li><div class="fake_button" onclick="deconnexion()">Deconnexion</div></li>
			  </ul>
		  </nav>
		  <section class="main_page">
			<h2>Welcome Administrator</h2>
			<div class="list">
					<SELECT name="nom" onchange="inscription_plage(this)">
						<?php
							if(isset($_SESSION['id']))
								include('includes/bdd.recupplage.inc.php'); 
						?>
					</SELECT>
			</div>
			<img  class="img_admin" src="pictures/plagebackground.jpg" alt="plage"/>
			<div class="inscription_plage">
				<?php
					include('includes/inscription_plage.inc.php');
				?>
			</div>
			<div class="description_plage">
				<?php
					if(isset($_SESSION['id']))
						include('includes/description.inc.php');
				?>
			</div>			
			<div class="creation_plage">
				<form method="post"  action="traitement_creation_plage.php">
					<p>Création d'une plage</p>
					<label for ="Nom_plage">Nom de la plage : </label><br/>
					<input id="Nom_plage" type="text" name="nom_plage" required><br/>
					<textarea class ="text_area" name="descriptif" maxlength="7000" placeholder="Enter text here..."></textarea>
					<input type="submit" value = "Créer">
					<input id="id_pseudo_creation" type="hidden" name="id_pseudo" value=<?php echo $_SESSION['id']; ?>/>
				</form>
			</div>
		</section>
	</div>
	<?php 
	include('includes/footer.inc.php');
	?>
  </body>
</html>