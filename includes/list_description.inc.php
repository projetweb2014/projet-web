<?php
	include('bddconnect.inc.php');
	if ($mysqli->connect_errno) {
		echo "Echec lors de la connexion à MySQL : (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	}else{
		$res2 = $mysqli->query("SELECT * from plage");
		if(!$res2){
			echo "<p>aucun resultat 2</p>";
		}else{
			echo '<p>Les plages disponibles :</p><br/>';
			echo '<table>';
			echo '<tr>';
				echo '<th>Nom de la plage</th>';
				echo '<th>Descriptif</th>';
			echo '</tr>';
			while($tuple=$res2->fetch_assoc()){
					echo '<tr>';
						echo '<td>'.htmlentities($tuple['nom_plage']).'</td>';
						echo '<td>'.htmlentities($tuple['Descriptif']).'</td>';
					echo '</tr>';
			}
			echo '</table>';
		}
	}
?>