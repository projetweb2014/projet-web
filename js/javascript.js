var showlog = 0;
var showins =0;

var showinscription_plage =0;
var showcreation_plage = 0;

var description_var = 0;

function login_form(){

	if (showlog==0){
		$('.article_place').hide();
		$('.connect').show();
		$('.connexion').show();
		$('.inscription').hide();
		$('.create').hide();
		showlog=1;
		showins=0;
		}
	else{
		$('.article_place').show();
		$('.connect').hide();
		$('.connexion').hide();
		$('.inscription').hide();
		$('.create').hide();
		showlog=0;
		showins=0;
		}
}

function inscription_plage_form(){

	if (showinscription_plage==0){
		$('.list').show();
		$('.creation_plage').hide();
		$('.inscription_plage').show();
		$('.img_admin').hide();
		$('.description_plage').show();
		$('.list_plage_description').hide();
		description_var=0;
		showinscription_plage=1;
		showcreation_plage=0;
		}
	else{
		$('.list').hide();
		$('.img_admin').show();
		$('.creation_plage').hide();
		$('.inscription_plage').hide();
		$('.description_plage').hide();
		$('.list_plage_description').hide();
		showinscription_plage=0;
		description_var=0;
		showcreation_plage=0;
		}
}

function creation_plage_form(){
	if(showcreation_plage==0){
		$('.list').hide();
		$('.img_admin').hide();
		$('.creation_plage').show();
		$('.inscription_plage').hide();
		$('.description_plage').hide();
		showcreation_plage=1;
		showinscription_plage=0;
		}
	else{
		$('.list').hide();
		$('.img_admin').show();
		$('.creation_plage').hide();
		$('.inscription_plage').hide();
		$('.description_plage').hide();
		showcreation_plage=0;
		
		showinscription_plage=0;	
	}
}

function description_plage_form(){
	if(description_var==0){
		$('.list_plage_description').show();
		$('.img_admin').hide();
		$('.inscription_plage').hide();
		$('.description_plage').hide();
		description_var=1;
		showinscription_plage=0;
		}
	else{
		$('.img_admin').show();
		$('.inscription_plage').hide();
		$('.list_plage_description').hide();
		$('.description_plage').hide();
		description_var=0;
		showinscription_plage=0;	
	}
}

function redirect_index(){
	setTimeout('top.location = \'index.php\'', 2000);
}

function redirect(){
	setTimeout('top.location = \'page_users.php\'', 2000);
}

function redirect_index_direct(){
	window.location.href="index.php";
}
function deconnexion(){
	window.location.href="traitement_deconnexion.php";
}

function inscription_form(){
	if(showins==0){
		$('.article_place').hide();
		$('.create').show();
		$('.inscription').show();
		$('.connect').hide();
		$('.connexion').hide();
		showins=1;
		showlog=0;
		}
	else{
		$('.article_place').show();
		$('.connect').hide();
		$('.connexion').hide();
		$('.inscription').hide();
		$('.create').hide();
		showins=0;
		showlog=0;	
	}
}
function verif_information(champ){
	if(champ.value.length == 0){
		champ.style.border = "1px solid #f00";
		return false;
	}else{
		return true;
	}
}

function inscription_plage(elem){
	var f = document.getElementById("name_plage");
	f.value= elem.options[elem.selectedIndex].text;
	var v = document.getElementById("value_plage");
	v.value = elem.options[elem.selectedIndex].value;
}

function verification_information(f){
	var pseudoOk = verif_information(f.pseudo);
	var passwordOk = verif_information(f.password);
	if(pseudoOk && passwordOk)
      return true;
   else
   {
	  alert("Veuillez remplir correctement tous les champs");
	  return false;
   }
}