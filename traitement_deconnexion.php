<?php session_start()?>
<!DOCTYPE php>
<html>
    <head>
        <title>Traitement</title>
        <meta charset="utf-8" />
		<link rel="stylesheet" href="css/reset.css">
		<link rel="stylesheet" href="css/style.css">
		<script type="text/javascript" src ="js/jquery.js"></script>
		<script type="text/javascript" src ="js/javascript.js"></script>
    </head>
    <body>
		<?php include('includes/header.inc.php'); ?>
        <h2>Deconnexion en cours</h2>
		<?php
			session_destroy();
			echo '<script>redirect_index_direct()</script>';
		?>
	<?php 
		include('includes/footer.inc.php');
	?>
    </body>
</html>

