<!DOCTYPE php>
<html>
    <head>
        <title>Traitement</title>
        <meta charset="utf-8" />
		<meta http-equiv="refresh" content="1;URL='./page_users.php'" />
		<link rel="stylesheet" href="css/reset.css">
		<link rel="stylesheet" href="css/style.css">
    </head>
    <body>
		<?php include('includes/header.inc.php'); ?>
		<?php include('includes/bdd_inscription.inc.php'); ?>
        <h2>Inscription en cours</h2>
        <p>
            Votre inscription est en cours <?php echo htmlentities($_POST['pseudo']); ?>. <br /> Merci de patientez. 
        </p>
    <?php 
        include('includes/footer.inc.php');
    ?>
    </body>
</html>