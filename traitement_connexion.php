<?php session_start()?>
<!DOCTYPE html>
<html>
    <head>
        <title>Traitement</title>
        <meta charset="utf-8" />
		<link rel="stylesheet" href="css/reset.css">
		<link rel="stylesheet" href="css/style.css">		
		<script type="text/javascript" src ="js/jquery.js"></script>
		<script type="text/javascript" src ="js/javascript.js"></script>
		
    </head>
    <body>
	<?php include('includes/header.inc.php'); ?>	
		<?php include('includes/bdd_connexion.inc.php'); ?>
		<?php 
			if( isset($_SESSION['id']) && ($_SESSION['id'] == $_POST['pseudo'])){
					echo '<h2>Connexion en cours</h2>';
					echo '<p>Votre connexion est en cours ';
					echo htmlentities($_POST['pseudo']);
					echo ' <br/> Merci de patientez.</p>';
				if(isset($_SESSION['droit']) && $_SESSION['droit'] == '0'){
					echo "'<script>setTimeout('top.location = \'page_admin.php\'', 1000);</script>'";
				}
				else{
					echo "'<script>setTimeout('top.location = \'page_users.php\'', 1000);</script>'";
				}
				
			}else{
				echo '<p>Erreur de connexion</p>';
				echo '<p>Redirection en cours vers la page précédente</p>';
				echo '<script>redirect_index();</script>';
			}
		?>
	<?php 
		include('includes/footer.inc.php');
	?>
    </body>
</html>