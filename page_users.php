<?php session_start()?>
<!Doctype html>
<html>
  <head>
    <title>Projet Informatique</title>
    <meta charset ="utf-8"/>
	<link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/style.css">
	<script type="text/javascript" src ="js/jquery.js"></script>
	<script type="text/javascript" src ="js/javascript.js"></script>
  </head>
  <body>
   <?php 
		if(!(isset($_SESSION['id']))){
			echo '<script> redirect_index_direct()</script>';
		}
   ?>
  <?php include('includes/header.inc.php'); ?>
    <div class="contenu">
		  <nav>
			  <ul id="Menu">
				<li><div class="fake_button" onclick="login_form()"><?php echo "Profil ".$_SESSION['id']?> </div></li>
				<li><div class="fake_button" onclick="inscription_plage_form()">Inscription à une plage</div></li>
				<li><div class="fake_button" onclick="description_plage_form()">Liste des plages</div></li>
				<li><div class="fake_button" onclick="deconnexion()">Deconnexion</div></li>
			  </ul>
		  </nav>
		  
		  <section class="main_page">
			<h2> Welcome <?php echo $_SESSION['id']; ?></h2>
			<div class="list">
					<SELECT name="nom" onchange="inscription_plage(this)">
						<?php
							if(isset($_SESSION['id']))
								include('includes/bdd.recupplage.inc.php'); 
						?>
					</SELECT>
			</div>
			<img  class="img_admin" src="pictures/plagebackground.jpg" alt="plage"/>
			<div class="inscription_plage">
				<?php  include('includes/inscription_plage.inc.php'); ?>
			</div>
			<div class="description_plage">
			<?php
				if(isset($_SESSION['id']))
					include('includes/description.inc.php');
			?>
			</div>
			<div class="list_plage_description">
				<?php
					if(isset($_SESSION['id'])){
						include('includes/list_description.inc.php');
					}
				?>
			</div>
		</section>
	</div>
	<?php 
		include('includes/footer.inc.php');
	?>
  </body>
</html>